# frozen_string_literal: true

class HomeController < ApplicationController
  def top; end

  # Add thêm action about
  def about; end
end
