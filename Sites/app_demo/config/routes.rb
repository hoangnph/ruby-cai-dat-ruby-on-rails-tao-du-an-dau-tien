# frozen_string_literal: true

Rails.application.routes.draw do
  get 'home/top'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  # Add thêm route about
  get 'about' => 'home#about'
end
